import numpy as np
from scipy import integrate

from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
#%matplotlib qt
plt.style.use(['ggplot'])


import sys
sys.path.append('/home/ar612/Documents/Python_modules/')
#sssys.path.append('/cluster/data/ar612/scripts/python/')
import mol

##### Defining functions ############################
def Vas(x,vmax,a,b):
    return (vmax/a**4)*(x**2-a**2)**2+b*x/a


def gauss(x,g,b,x0):
    return g*np.e**(-b*(x-x0)**2)
def sum_gauss(x,g,b,x0):
    sum=0
    for i in range(0,len(g)):
        sum=sum + gauss(x,g[i],b[i],x0[i])
    return sum


def integ(f,xi,xf):
    i=integrate.quad(f,xi,xf)
    return i

######### Defining gaussians coeficients ####################
vmax=0.0002*630
a=2
wf_scale=vmax

g=np.array([31.00001469*vmax,-1.52881858*vmax,-1.52881858*vmax,31.00001469*vmax,1.34845505*vmax])  ##/*!< Gaussian potential expansion height coefficients */
b=np.array([1.39712198*pow(a,-2),1.65840237*pow(a,-2),1.65840237*pow(a,-2),1.39712198*pow(a,-2),0.*pow(a,-2)]) ##     /*!<  Gaussian potential expansion wide coefficients */
xp=np.array([-2.98085171*a,-1.14213435*a,1.14213435*a,2.98085171*a,0.*a])      ##   /*!<  Gaussian potential expansion center coefficients */




######## Ploting the Potential ##################################
xmax=6
x=np.linspace(-xmax,xmax,1000)
#plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp))

alfa=0.192892
x0=-4.26568

#%matplotlib qt
plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp),x, gauss(x,(2*alfa/np.pi)**(1/2.), 2*alfa , x0) )





###%%
#%matplotlib qt
### Creating the subfigures
fig=plt.figure(1,figsize=(10,8))
plt.style.use(['ggplot'])


###Ploting the first figure
inp_file_pattern="results/t_Yt_$$.txt"
efield_file_pattern="results/t_et_$$.txt"
inp_file=inp_file_pattern.replace("$$",str(0))
efield_file=efield_file_pattern.replace("$$",str(0))

tY,a1,b1,x01,p01,a2,b2,x02,p02=mol.fast_load(inp_file,np.array([0,1,2,3,4,5,6,7,8]),1000).T
#te,et=mol.fast_load(efield_file,np.array([0,1]),1000).T

fs2au=41.3414

tY=tY/fs2au
#te=te/fs2au

tY=np.array(map(lambda x: round(x,4),tY))
eax_lim=0.02
xlim=2*a
vlim=4*vmax
plt.subplot(111)
plt.axis([-xlim, xlim, 0, 4*vmax])
c1='b'
c2='r'
label1='psi'
label2='xi'

plt.plot(x,sum_gauss(x,g,b,xp), color='tab:green' )
plt.plot(x, wf_scale*gauss(x,(2*a1[0]/np.pi)**(1/2.), 2*a1[0] , x01[0]),color=c1,label=label1)
plt.plot(x,wf_scale*gauss(x,(2*a2[0]/np.pi)**(1/2.), 2*a2[0] , x02[0]),color=c2,label=label2)
plt.xlabel("x (a0)",fontweight='bold')
plt.ylabel("V (Eh)",fontweight='bold')
plt.legend(loc='upper center')
plt.tick_params(labelsize=14)
#plt.subplot(122)
#plt.axis([min(abs(te)), max(abs(te)), -eax_lim, eax_lim])
# plt.plot(te,et)
# plt.xlabel("t (fs)",fontweight='bold')
# plt.ylabel("E (au)",fontweight='bold')
# plt.tick_params(labelsize=14)


## Set slide axes
axt_val = plt.axes([0.25, .03, 0.50, 0.02])
axi_val = plt.axes([0.25, .00, 0.50, 0.02])
# Slider time
dt=round(abs(tY[1]-tY[0]),4)
imax=int(sys.argv[1])
st_val = Slider(axt_val, 't (fs)', 0, abs(tY[0]), valinit=abs(tY[0]),valstep=dt)
si_val = Slider(axi_val, 'i', 0, imax,valinit=0,valstep=1)


def update(val):

    i=int(si_val.val)

    inp_file_pattern="results/t_Yt_$$.txt"
    inp_file=inp_file_pattern.replace("$$",str(i))


    tY,a1,b1,x01,p01,a2,b2,x02,p02=mol.fast_load(inp_file,np.array([0,1,2,3,4,5,6,7,8]),1000).T

    fs2au=41.3414

    tY=tY/fs2au
    tY=np.array(map(lambda x: round(x,4),tY))

    t_val = list(tY).index(round(st_val.val,4))

    if(i%2==0):
        c1='r'
        c2='b'
        label1='xi'
        label2='psi'
    if(i%2==1):
        c1='b'
        c2='r'
        label1='psi'
        label2='xi'

    plt.subplot(111)
    plt.cla()
    plt.axis([-xlim, xlim, 0, vlim])
    plt.plot(x,sum_gauss(x,g,b,xp), color='tab:green')
    plt.plot(x, wf_scale*gauss(x,(2*a1[t_val]/np.pi)**(1/2.), 2*a1[t_val] , x01[t_val]) ,color=c1,label=label1)
    plt.plot(x, wf_scale*gauss(x,(2*a2[t_val]/np.pi)**(1/2.), 2*a2[t_val] , x02[t_val]) ,color=c2,label=label2)
    plt.legend(loc='upper center')
    plt.xlabel("x (a0)",fontweight='bold')
    plt.ylabel("V (Eh)",fontweight='bold')
    plt.tick_params(labelsize=14)

def update_i(val):

    i=int(si_val.val)

    inp_file_pattern="results/t_Yt_$$.txt"
    efield_file_pattern="results/t_et_$$.txt"
    inp_file=inp_file_pattern.replace("$$",str(i))
    efield_file=efield_file_pattern.replace("$$",str(i))

    tY,a1,b1,x01,p01,a2,b2,x02,p02=mol.fast_load(inp_file,np.array([0,1,2,3,4,5,6,7,8]),1000).T
    #te,et=mol.fast_load(efield_file,np.array([0,1]),1000).T

    fs2au=41.3414

    tY=tY/fs2au
    #te=te/fs2au
    tY=np.array(map(lambda x: round(x,4),tY))


    st_val.set_val((i+1)%2*max(abs(tY)))
    t_val = list(tY).index(round(st_val.val,4))

    if(i%2==0):
        c1='r'
        c2='b'
        label1='xi'
        label2='psi'
    if(i%2==1):
        c1='b'
        c2='r'
        label1='psi'
        label2='xi'


    plt.subplot(111)
    plt.cla()
    plt.axis([-xlim, xlim, 0, vlim])
    plt.plot(x,sum_gauss(x,g,b,xp), color='tab:green')
    plt.plot(x, wf_scale*gauss(x,(2*a1[t_val]/np.pi)**(1/2.), 2*a1[t_val] , x01[t_val]) ,color=c1,label=label1)
    plt.plot(x, wf_scale*gauss(x,(2*a2[t_val]/np.pi)**(1/2.), 2*a2[t_val] , x02[t_val]) ,color=c2,label=label2)
    plt.xlabel("x (a0)",fontweight='bold')
    plt.ylabel("V (Eh)",fontweight='bold')
    plt.legend(loc='upper center')
    plt.tick_params(labelsize=14)
    # plt.subplot(122)
    # plt.cla()
    # plt.axis([min(abs(te)), max(abs(te)), -eax_lim, eax_lim])
    # plt.plot(te,et)
    # plt.xlabel("t (fs)",fontweight='bold')
    # plt.ylabel("E (au)",fontweight='bold')
    # plt.tick_params(labelsize=14)


# call update function on slider value change
st_val.on_changed(update)
si_val.on_changed(update_i)

plt.show()
###%%
