
class dif_eq
{
  private : 
      int for_back; //This is an indicator if I'm propagating forward (1) or backwards (-1). This is important for E12. In the forward propagation 
      string im_re;   //If I do imaginary time (1) or real time(2) propagation
      string int_normal;
      double grad_step;


      //This function is the previous electric field. I define it in the initialization list of the class constructor
      std::function<double(double)> E; //Declaring the function E(t)

      //These arrays are to storage the dipole matrix and eigenvalues
      vector<vector<double>> mu;
      vector<vector<double>> eigen;
      int Emax;


    template<class type1, class type2>
    void H_dot_a(type1 *b, type2 **H, type1 *a, size_t Emax)
    {
        for (size_t i=0; i<Emax; i++)
        {
            b[i]=0;
            b[i+Emax]=0;
            for (size_t j=0; j<Emax; j++)
            {
                b[i]=b[i]+H[i][j]*a[j+Emax];
                b[i+Emax]=b[i+Emax]-H[i][j]*a[j];
            }
        }
    }

    template<class type1, class type2>
    void H_dot_a(type1 *b, type2 **muEcos, type2 **muEsin, type1 *a, size_t Emax)
    {
        for (size_t i=0; i<Emax; i++)
        {
            b[i]=0;
            b[i+Emax]=0;
            for (size_t j=0; j<Emax; j++)
            {
                b[i]=b[i] - ( muEcos[i][j]*a[j+Emax] + muEsin[i][j]*a[j] );
                b[i+Emax]=b[i+Emax] + ( muEcos[i][j]*a[j] - muEsin[i][j]*a[j+Emax] );
            }
        }
    }



    public:
      //static const int n_monit = 30;

      dif_eq(vector<vector<double>> mu, vector<vector<double>> eigen, string im_re, string int_normal, std::function<double(double)> E) : mu(mu), eigen(eigen), im_re(im_re), int_normal(int_normal), E(E)
      {
        Emax=eigen.size();
      }




      /**
       * @brief This is an overload of the () operator so I can pass this function to Solve_DEq
       * 
       * @param f This is a pointer where I store the evaluation of the function
       * @param t The current instant of time 
       * @param Y The current values of the wavefunction parameters
       * @param n_eq The number of equations/parameters that I have
       */
      void operator()(double *der, double *monit, size_t n_monit, double t, double *Y, size_t n_eq)
      {

        if ((im_re != "im" && im_re != "re"))
        {
          throw invalid_argument("!!!!!!!!!! im_re have to be 1 or 2 in class dif_eq !!!!!!!!!!!!!!!!");
        }
 
        //print_matrix(eigen,Emax,Emax);

        double** H;
        double** muEcos;
        double** muEsin;
        double wij;
        matrix_allocation(muEcos,Emax,Emax);
        matrix_allocation(muEsin,Emax,Emax);
        matrix_allocation(H,Emax,Emax);



        if (im_re == "im")
        {

        }

        if (im_re == "re" and int_normal == "int")
        {
            for (size_t i=0; i<Emax; i++)
            {
                for (size_t j=0; j<Emax; j++)
                {
                    wij=eigen[i][i]-eigen[j][j];
                    muEcos[i][j]=mu[i][j]*E(t)*cos(wij*t);
                    muEsin[i][j]=mu[i][j]*E(t)*sin(wij*t);
                    //cout<<endl<<H[i][j]<<"  "<<eigen[i][j]<<"  "<<mu[i][j]<<"  "<<E(t)<<endl;
                }
            }
            H_dot_a(der, muEcos,muEsin, Y,Emax);
        }

        if (im_re == "re" and int_normal == "normal")
        {
          for (size_t i=0; i<Emax; i++)
          {
              for (size_t j=0; j<Emax; j++)
              {
                  H[i][j]=eigen[i][j] - mu[i][j]*E(t);
                  //cout<<endl<<H[i][j]<<"  "<<eigen[i][j]<<"  "<<mu[i][j]<<"  "<<E(t)<<endl;
              }
          }
          H_dot_a(der, H, Y,Emax);
        }


        // matrix_delete(H,Emax);
        matrix_delete(muEcos,Emax);
        matrix_delete(muEsin,Emax);
        matrix_delete(H,Emax);

        monit[0]=E(t);
        for (size_t k=0; k<Emax;k++)
        {
          monit[k+1]=Y[k]*Y[k] + Y[k+Emax]*Y[k+Emax];
          //cout<<endl<<monit[k];
        }
        //monit[30] = Y[0];
        //monit[31] = Y[1];


        // monit[0] = E(t);
        // monit[1] = Y[0]*Y[0] + Y[0+Emax]*Y[0+Emax];
        // monit[2] = Y[1]*Y[1] + Y[1+Emax]*Y[1+Emax];
        // monit[3] = Y[2]*Y[2] + Y[2+Emax]*Y[2+Emax];
        // monit[4] = Y[3]*Y[3] + Y[3+Emax]*Y[3+Emax];
        // monit[5] = Y[4]*Y[4] + Y[4+Emax]*Y[4+Emax];
        // monit[6] = Y[5]*Y[5] + Y[5+Emax]*Y[5+Emax];
   
          
      }

};
