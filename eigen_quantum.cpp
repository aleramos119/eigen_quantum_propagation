/*!
   \file main.cpp
   \brief This file contains the main function of the program.

          In this main function I gather the initial input for the program.
          Here is also defined the function func which is the function that defines the system of diferential equation
          that will be solved by the Adams-Moulton metod, and the function set_initial_condition which creats an array
          for the initial values of the wave function parameters
   \author Alejandro Ramos
   \date 21/05/2019
*/




#include <functional>
#include <gsl/gsl_math.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>
#include <limits>



//#include "../include/nvwa/debug_new.h"
#include "../include/linear_systems.h"//Esta biblioteca no pertenece a C++

#include "class_include.h"
#include "gen_funct.h"


using namespace std;
using namespace std::placeholders;


class System
{
public:
    vector<vector<double>> mu;  //Dipole matrix
    vector<vector<double>> E;   //Diagonal version of eigen energies

    size_t Emax;

    //Constructors
    System(){};
    System(string E_file, string mu_file, size_t Emax) : E_file(E_file), mu_file(mu_file), Emax(Emax)
    {
        //E_vec = import_vector_from_txt_file(E_file);
        //E=vec2diag(E_vec);

        E= import_matrix_from_txt_file(E_file);
        mu= import_matrix_from_txt_file(mu_file);

        resize_matrix(E,Emax,Emax);
        resize_matrix(mu,Emax,Emax);
    };
    //Destructors
    ~System(){}; // This is the destructor declaration

private:
    vector<double> E_vec; //Vector version of eigenenergies
    string E_file;
    string mu_file;
};


class User_Param
{
    public:

        //State information
        size_t Emax;
        size_t nstates;
        vector<double> init_state;

        //Propagation parameters
        size_t N;
        size_t step;
        double ti;
        double dt;
        string im_re;
        string int_normal;

        //Field parameters
        string read_e_file;
        double E0;
        double w;
        double phi;
        string efield_file;



        //System class
        System sys;


        User_Param()
        {
            string aux;
            //Initial and final state information

            cout << "Enter Emax: ";
            cin >> aux;
            cin >> Emax;
            cout <<Emax<<endl; 
            nstates=2*Emax;

            

            //Algorithm parameters
            cout << "Enter number of steps (N): ";
            cin >> aux;
            cin >> N;
            cout<< N<< endl;

            cout << "Enter number of steps to print: ";
            cin >> aux;
            cin >> step;
            cout<< step<< endl;

            cout << "Enter initial time (ti): ";
            cin >> aux;
            cin >> ti;
            cout << ti<<endl;
           

            cout << "Enter time step (dt): ";
            cin >> aux;
            cin >> dt;
            cout<<dt<<endl;
            

            cout << "Enter imaginary (im) or real (re) propagation: ";
            cin >> aux;
            cin >> im_re;
            cout<<im_re<<endl;


            cout << "Enter interaction (int) or normal (normal) propagation: ";
            cin >> aux;
            cin >> int_normal;
            cout<<int_normal<<endl;
            
    
            cout << "Enter initial state file: ";
            cin >> aux;
            cin >> init_file;
            cout<<init_file<<endl;
            init_state=import_vector_from_txt_file(init_file);

            if (init_state.size() < nstates)
                throw logic_error("*********** ERROR *************   User_Param: the dimension of init_state must greater than 2*Emax");
            normalize(init_state);

            init_state.resize(nstates);


            //System class
            cout << "Enter Hamiltonian matrix file: ";
            cin >> aux;
            cin >> H_file;
            cout<< H_file<<endl;
            

            cout << "Enter Dipole matrix file: ";
            cin >> aux;
            cin >> mu_file;
            cout<< mu_file<<endl;

            cout << "Read efield? (yes/no): ";
            cin >> aux;
            cin >> read_e_file;
            cout<< read_e_file<<endl;

            cout << "Enter E0: ";
            cin >> aux;
            cin >> E0;
            cout<< E0<<endl;

            cout << "Enter w: ";
            cin >> aux;
            cin >> w;
            cout<< w<<endl;

            cout << "Enter phi: ";
            cin >> aux;
            cin >> phi;
            cout<< phi<<endl;

            cout << "Enter efield file: ";
            cin >> aux;
            cin >> efield_file;
            cout<< efield_file<<endl;
 

            sys=System(H_file,mu_file,Emax);

        };

    private:
        string init_file;

        string H_file;
        string mu_file;

};





void vec2diag(double* vec, double** &diag, size_t n)
{
    matrix_allocation(diag,n,n);
    matrix_cero_initialization(diag,n,n);

    for(size_t i=0; i<n;i++)
    {
        diag[i][i]=vec[i];
    }

}


void write_monit_to_file(double *t, size_t N, double **monit, size_t n_monit, const char *file_name, string header) //Imprime una matriz
{
  FILE *data;

  data=fopen(file_name, "w");
  //cout << header;
  fprintf(data, "%s", header.c_str());

  for (int i=0; i < N; i++)
  {
    fprintf(data, "\n%e  ", t[i]);
    for (int j = 0; j < n_monit; j++)
    {
      fprintf(data, "%e  ", monit[i][j]);
    }
  }

  fclose(data);
}


void order_field(double *t_asc, double *e_asc, double *t, double *e_t, size_t N)
{
  //If time is already in ascending order, remains the same for the spline
  if (t[1] > t[0])
  {
    for (int j = 0; j < N ; j++)
    {
      t_asc[j] = t[j];
      e_asc[j] = e_t[j];
    }
  }
  //If time is NOT in ascending order, I invert it
  else if (t[1] < t[0])
  {
    for (int j = 0; j < N ; j++)
    {
      t_asc[j] = t[N -1 - j];
      e_asc[j] = e_t[N -1 - j];
    }
  }
  //This are the only 2 possibilities, so else I throw an error
  else
  {
    throw invalid_argument("!!!!!!!!!! Field must be in strictly ascending or descending order !!!!!!!!!!!!!!!!");
  }
}

/**
 * @brief This is a function that returns the differential equation class. It converts the field in strictly ascending order and creats the 
 *        electric field spline to pass to the differential equation
 * 
 * @param acc An accelerator pointer for the spline
 * @param efield_spline The spline pointer
 * @param im_re If it is imaginary or real propagation
 * @param t The pointer to the array of time instants. It has size N+1
 * @param e_t The pointer to the array with electrical field corresponding to time instant t/ It has size N+1
 * @param h The step to calculate the numerical derivative of the field. Must be small for the derivative to be accurate, but not to much that numerical errors start to appear 
 * @param N The number of points where the wavefunction is calculated. Icluding the initial point there are N+1.
 * @param T The lenght in time of the simulation. T=N*dt
 * @param file_name where I will output the spline of the field
 * @param wf If I'm performing a Psi propagation or a Chi propagation. This is important for the sign of the field.
 * @return dif_eq Returns the differential equation class.
 */
dif_eq construct_dif_eq(gsl_interp_accel *&acc, gsl_spline *&efield_spline, string im_re, string int_normal, double *t, double *e_t, size_t N, int for_back, vector<vector<double>> mu, vector<vector<double>> eigen)
{
  size_t Emax=mu.size();
  double* t_asc; //Ascending time
  double* e_asc; //Corresponding ascending time field

  //Allocating memory
  vector_allocation(t_asc,N+1);
  vector_allocation(e_asc,N+1);

  order_field(t_asc,e_asc,t,e_t,N+1);
  
  //Allocating the spline accelerator
  acc = gsl_interp_accel_alloc();                              //Alocating accelerator
  efield_spline = gsl_spline_alloc(gsl_interp_cspline, N + 1); //Alocating cubic spline

  // Create the spline with the field data
  gsl_spline_init(efield_spline, t_asc, e_asc, N + 1);

  // Defining the electrical field function
  std::function<double(double)> E = std::bind(gsl_spline_eval, efield_spline, _1, acc); 

  // Defining the diferential equation with the field
  dif_eq func(mu, eigen, im_re, int_normal, E);

  // double *t_spline; //Ascending time
  // double *e_spline; //Corresponding ascending time field

  // //Allocating memory
  // vector_double_allocation(t_spline, 3 * N + 1);
  // vector_double_allocation(e_spline, 3 * N + 1);

  // for (int i=0; i<3*N+1;i++)
  // {
  //   t_spline[i]=i*T/(3*N);
  //   e_spline[i]=E(t_spline[i]);
  // }

  // //write_field_to_file(t_spline, e_spline, 3*N + 1, file_name, "# t (au)    e (Eh/(e a0))");

  //Deleting the allocated pointers
  delete[] e_asc;
  delete[] t_asc;

  return func;
}


// void iter_monitor(int iter, double *t, double **monit, double a0, const char *file_name, size_t N, size_t n_monit, double *y, double *yf, dif_eq func, size_t n_eq)
// {

//   double *t_asc; //Ascending time
//   double *e_asc; //Corresponding ascending time field
//   double *e_2; // The square of the electric field
//   double *e_t_out; //The field taken from monitor matrix

//   double integral; //Here I storage the integral of the field from 0 to T
//   double over; // Here I storage the overlap with the final state

//   //Allocating memory
//   vector_allocation(t_asc, N + 1);
//   vector_allocation(e_asc, N + 1);
//   vector_allocation(e_2,N+1);
//   vector_allocation(e_t_out, N + 1);

//   //Extracting the field from monit
//   take_col(e_t_out, monit, N + 1, n_monit, 0);

//   //Ordering the time in ascending order.
//   order_field(t_asc, e_asc, t, e_t_out, N + 1);

//   for(int i =0; i< N+1;i++)
//   {
//     e_2[i]=pow(e_asc[i],2.);
//   }


//   integral=a0*simpson(e_2,t_asc[0],t_asc[N],N);
//   over=func.overlap(y,yf,n_eq);
//   double J=integral-over;


//   FILE *data;

//   if (iter==0)
//   {
//     data = fopen(file_name, "w");
//     fprintf(data, "%s", "# iter   int_e  over   over+a0*int_e");
//     fprintf(data,"\n %i   %e   %e   %e", iter, integral, over,J);
//   }
//   else
//   {
//     data = fopen(file_name, "a");
//     fprintf(data, "\n %i  %e    %e   %e", iter, integral, over, J);
//   }

//   fclose(data);

//   delete[] e_t_out;
//   delete[] t_asc;
//   delete[] e_asc;
// }

// /**
//  * @brief This function allocates the initial field arrays. And choose from reading form a file or compute the field with a cos function.
//  *        If readed from a file the values of ti, dt and N will be overwritted with the values from the file
//  * 
//  * @param t_in The array to be allocated that stores the time instants
//  * @param e_t_in The field at each instant of time  
//  * @param ti The initial time
//  * @param dt The time interval
//  * @param N The total number of steps. len(ti)=N+1 because includes the initial point
//  * @param E0 The amplitud of the field if it is generated
//  * @param w The frequancy of the field if it is generated
//  * @param phi The phase of the field if it is generated
//  * @param read_e_file Selects if read from file(1) or not(anything else)
//  * @param efield_file The file to read the initial field
//  */
void initial_e_field(double *&t_in, double *&e_t_in, double &ti, double &dt, size_t &N, double E0, double w, double phi, string read_e_file, string efield_file)
{

  size_t rows_read; //Number of rows in the file 
  size_t columns_read; //Number of columns in the file

  double tf;

  double* aux_time;

  double** matrix;

  double* t_read; //Read time
  double* e_read; //Corresponding read field

  double* t_asc; //Ascending time
  double* e_asc; //Corresponding ascending time field

  gsl_interp_accel *acc;      // A pointer to an interpolation accelerator
  gsl_spline *efield_spline;  // A pointer to the interpolation



  if (read_e_file=="yes")//If read from file is activated
  {
    //Import the file matrix
    import_matrix_from_txt_file(efield_file,matrix,rows_read,columns_read);

    //Allocating the vectors
    vector_allocation(t_read, rows_read);
    vector_allocation(e_read, rows_read);
    vector_allocation(t_asc,rows_read);
    vector_allocation(e_asc,rows_read);
    vector_allocation(aux_time, rows_read);
    

    //Setting read the vectors values
    take_col(t_read, matrix, rows_read, columns_read, 0);
    take_col(e_read, matrix, rows_read, columns_read, 1);

    //Ordering the field in ascending order
    order_field(t_asc,e_asc,t_read,e_read,rows_read);


    //Allocating the spline accelerator
    acc = gsl_interp_accel_alloc();                              //Alocating accelerator
    efield_spline = gsl_spline_alloc(gsl_interp_cspline, rows_read); //Alocating cubic spline

    // Create the spline with the field data
    gsl_spline_init(efield_spline, t_asc, e_asc, rows_read);

    // ti=min(t_in[0],t_in[N+1]);ti=min(t_in[0],t_in[N+1]);Defining the electrical field function
    std::function<double(double)> E = std::bind(gsl_spline_eval, efield_spline, _1, acc); 

    ti=t_asc[0];
    tf=t_asc[rows_read-1];

    N=ceil((tf-ti)/dt);//Here I calculate an approximate N

    if(N%2==1)// Here I make sure that N (the number of steps is even for sipson integrator)
    {
      N=N+1;
    }

    dt=(tf-ti)/N; //Calculating the final dt

    

    //Allocating memory
    vector_allocation(t_in, N + 1);
    vector_allocation(e_t_in, N + 1);


    for (int i=0; i<N+1;i++)
    {
      t_in[i]=ti+i*dt;
      e_t_in[i]=E(t_in[i]);
    }
    

    delete[] matrix;
    
    
  }
  else
  {
    //Allocating the vectors
    vector_allocation(t_in, N + 1);
    vector_allocation(e_t_in, N + 1);

    for (size_t i = 0; i < N + 1; i++)
    {
      t_in[i] = ti + i * dt;
      e_t_in[i] = E0 * cos(w * (ti + i * dt) + phi)*pow(sin(PI*(ti + i * dt)/(ti + N * dt) - ti),2);
    }
  }
  

  
  
}


  // template<class type1, class type2>
  // type1 cross_correl_2(type1 *a, type2 *b, size_t Emax)
  // {
  //   type1 re=0;
  //   type1 im=0; 
  //   for (size_t i=0; i<Emax; i++)
  //   {
  //       re = re + a[i]*b[i] + a[i+Emax]*b[i+Emax];
  //       im = im + a[i]*b[i+Emax] - a[i+Emax]*b[i];
  //   }

  //   return pow(re,2.)+pow(im,2.);
  // }

  // template<class type1>
  // type1 norm(type1 *a, size_t Emax)
  // {
  //     return pow(cross_correl_2(a,a,Emax),1/4.);
  // }

  // template<class type1>
  // void normalize(type1 *a, size_t Emax)
  // {
  //   type1 norm_a=norm(a,Emax);
  //   for (size_t i=0; i<2*Emax; i++)
  //   {
  //     a[i]=a[i]/norm_a;
  //   }
  // }

  template<class type1>
  void vector2double(vector<type1> a,type1 *&b)
  {
    vector_allocation(b,a.size());
    for (int i=0; i<a.size();i++)
    {
      b[i]=a[i];
    }
  }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    int main()
{

    User_Param uparam;



    //Initial field
    double *t_in; // Here I storage the time
    double *e_t_in; // Here I storage the time dependence of the input electrical field

    //Initializing the field. This function overwrites ti, dt and N if the field is read from file
    initial_e_field(t_in, e_t_in, uparam.ti, uparam.dt, uparam.N, uparam.E0, uparam.w, uparam.phi, uparam.read_e_file, uparam.efield_file);

    //Allocating initial and final vectors where I store initial and final set of parameters
    double *y0;
    double *yf;
    double *yzero;
    double *yaux;
    double *lag_init;

    double *yin;
    double *yout;

    vector_allocation(y0, uparam.nstates);
    vector_allocation(yf, uparam.nstates);
    vector_allocation(yzero, uparam.nstates);
    vector_allocation(yaux, uparam.nstates);
    vector_allocation(lag_init, uparam.nstates);

    vector_allocation(yin, 2 * uparam.nstates);
    vector_allocation(yout, 2 * uparam.nstates);

    //This is a dummy zero state to use in the first propagation
    vector_cero_initialization(yzero, uparam.nstates);

    double for_back;  //A flag that identifies forward (1) or backwards (-1) popagation
    double tf = uparam.N * uparam.dt; //The final instant of time

    double *t_out;   // Here I storage the time
    double *e_t_out; // Here I storage the time dependence of the output electrical field

    double **monit;  // This is a matrix that stores the evaluation of some multidimentional function of the parameters every time step
    size_t n_monit = uparam.Emax + 1;//dif_eq::n_monit; // Dimension of the multidimensional function

    gsl_interp_accel *acc;      // A pointer to an interpolation accelerator
    gsl_spline *efield_spline;  // A pointer to the interpolation


    vector_allocation(t_out, uparam.N + 1);
    vector_allocation(e_t_out, uparam.N + 1);



    //This where I store a user defined function evaluation as a function of time
    matrix_allocation(monit, uparam.N + 1, n_monit);

    //Setting the initial and final state
    vector2double(uparam.init_state,y0);
    vector_cero_initialization(yf,uparam.nstates);
    print_vector(y0,uparam.nstates);
    


    // ////////////////////////////////////////////////////////////////////////////////////////////
    // //////////  Performing first propagation  //////////////////////////////////////////////////
    // ////////////////////////////////////////////////////////////////////////////////////////////

    // Defining where I will output the data
    ostringstream monitor_file;
    monitor_file << "results/t_monitor_" << 0 << ".txt";
    ostringstream monitor_file_header;
    monitor_file_header << "#t(au)          E";
    for (int i=0; i < uparam.Emax; i++ )
    {
      monitor_file_header<<"           Pop"<<i;
    }
   

    ostringstream out_file;
    out_file << "results/t_Yt_" << 0 << ".txt";


    //Constructing first differential equation
    for_back=1;
    dif_eq func =construct_dif_eq(acc, efield_spline, uparam.im_re, uparam.int_normal, t_in, e_t_in, uparam.N, for_back, uparam.sys.mu, uparam.sys.E);

    // if (im_re == "im")
    // {
    //   //Solving differential equations for imaginary time.
    //   Solve_DEq('a', func, t_out, monit, n_monit, y0, yout, ti, N, for_back*dt, step, out_file.str().c_str(), print, 2 * n_eq);
    // }
    if (uparam.im_re == "re")
    {
      //Solving differential equations for real time
      Solve_DEq('a', func, t_out, monit, n_monit, y0, yout, uparam.ti, uparam.N, for_back*uparam.dt, uparam.step, out_file.str().c_str(), 1, uparam.nstates);
      take_col(e_t_out, monit, uparam.N + 1, n_monit, 0);
    }


    //Writing monitor for the first iteration
    write_monit_to_file(t_out, uparam.N + 1, monit, n_monit, monitor_file.str().c_str(), monitor_file_header.str().c_str());

    // // take(yaux, yout, 0, 4);
    // // iter_monitor(0, t_out, monit, a0, iter_monit_file.str().c_str(), N, n_monit, yaux, yf, func, n_eq);

    // // double init_time; //This is defined to alternate between the initial time and the final time

    // // gsl_spline_free(efield_spline); //Free the spline pointer
    // // gsl_interp_accel_free(acc);     //Free the accelerator pointer

    

    // // ////////////////////////////////////////////////////////////////////////////////////////////
    // // //////////  Performing iteration propagation  //////////////////////////////////////////////
    // // ////////////////////////////////////////////////////////////////////////////////////////////
    // // for (int i = 1; i < iter; i++)
    // // {
    // //   // Defining where I will output the data
    // //   // ostringstream spline_file;
    // //   // spline_file << "results/spline_" << i << ".txt";

    // //   // i odd => backward propagation (for_back=-1)  ,   i even => forward propagation (for_back=1)
    // //   for_back = (2 * ((i+1) % 2) - 1);
    // //   if(for_back==1)
    // //   {
    // //     //For forward propagation I don't need to propagate the Lagrange multipliers, that's why I initialize them with zero
    // //     join_vectors(yin, y0, 0, 4, yzero, 0, 4);
    // //     init_time=ti;
    // //   }
    // //   else if (for_back == -1)
    // //   {
    // //     take(yaux, yout, 0, 4);

    // //     func.lagrange_projection(lag_init,yaux,yf,4);

    // //     join_vectors(yin, yaux, 0, 4 , lag_init,0,4);

    // //     init_time = tf;
    // //   }


    // //   //Defining the set of differential equations
    // //   func = construct_dif_eq(acc, efield_spline, q, m, vmax, a, im_re, t_out, e_t_out, a0, N, tf, dt, for_back,grad_step);


    // //   // Defining where I will output the data
    // //   ostringstream monitor_file;
    // //   monitor_file << "results/t_monitor_" << i << ".txt";

    // //   ostringstream out_file;
    // //   out_file << "results/t_Yt_" << i << ".txt";

    // //   ostringstream gradn_file;
    // //   gradn_file << "results/gradn_" << i << ".txt";

    // //   ostringstream gradn_1_file;
    // //   gradn_1_file << "results/gradn_1_" << i << ".txt";

    // //   ostringstream sn_file;
    // //   sn_file << "results/sn_" << i << ".txt";

    // //   ostringstream sn_1_file;
    // //   sn_1_file << "results/sn_1_" << i << ".txt";

    // //   //Solving differential equations
    // //   Solve_DEq('a', func, t_out, monit, n_monit, yin, yout, init_time, N, for_back * dt, step, out_file.str().c_str(), print, 2 * n_eq);
    // //   take_col(e_t_out, monit, N + 1, n_monit, 0);
      

    // //   if( for_back == -1)// I only update the field for the backward propagation
    // //   {
    // //     take_col(gradn, monit, N + 1, n_monit, 1);
    // //     if (i == 1) //The new output field (input for next iteration) is given by a stepest descendent step for the first iteration
    // //     {
    // //       stepest_step(e_t_out, e_t_out, gradn, an_1, N + 1);
    // //       scale_vec(gradn_1,1,gradn,N+1);
    // //       scale_vec(sn_1,1,gradn_1,N+1); //For gradient descendent method the descent direction is iqual to the gradient
    // //     }
    // //     else
    // //     {
    // //       //stepest_step(e_t_out, e_t_out, gradn, an_1, N + 1);
    // //       //write_e_field_to_file(t_out, N + 1, gradn, gradn_file.str().c_str(), "#tn   gradn");
    // //       //write_e_field_to_file(t_out, N + 1, gradn_1, gradn_1_file.str().c_str(), "#tn   gradn_1");

    // //       //write_e_field_to_file(t_out, N + 1, sn_1, sn_1_file.str().c_str(), "#tn   sn_1");
    // //       cg_step(e_t_out, e_t_out, gradn, gradn_1, sn_1, an_1, N + 1);
    // //       //write_e_field_to_file(t_out, N + 1, sn_1, sn_file.str().c_str(), "#tn   sn");
    // //       scale_vec(gradn_1, 1, gradn, N + 1);
    // //     }
        

    // //   }


      


    // //   //Writing monitor
    // //   write_monit_to_file(t_out, N + 1, monit, n_monit, monitor_file.str().c_str(), monitor_file_header);

    // //   if (for_back==1)
    // //   {
    // //     take(yaux,yout,0,4);
    // //     iter_monitor(i, t_out, monit, a0, iter_monit_file.str().c_str(), N, n_monit,yaux,yf,func,n_eq);
    // //   }

    // //   cout<<endl<<endl<<"!!!!!!  Iteration: "<<i<< endl;
      

    // //   gsl_spline_free(efield_spline); //Free the spline pointer
    // //   gsl_interp_accel_free(acc);     //Free the accelerator poiunter
    // //   }

}
